[Glossary](https://kubernetes.io/docs/reference/glossary/?fundamental=true)

[Horizontal pod autoscaling](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)

[Debugging pods](https://kubernetes.io/docs/tasks/debug/debug-application/debug-pods/)
