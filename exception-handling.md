Checklist:
1. Do we want to handle a local piece of code that may result in an exception, that can be locally handled?
Yes. We only want to propagate the exception if we cannot handle it at the current level. We should not propagate exceptions with any other reason than "I cannot/do not know how to handle it here".

2. Do we want the whole program to crash in case this exception occurs, such that we alert the team about it? 
Probably not. We have ERROR_CODES_FOR_ALERT, and whenever we have an `Alert:` in a log, the azure monitoring will send us a message on slack.


3. Do we want to handle the errors at main level?
Probably not, unless they cannot be handled at any other level.

4. Should we add a try-except to every single rest api endpoint, such that if an exception happens we will always return an http status?
If an unhandled exception happens at a lower level, then we need to catch it and return and error code to the user, yes.

## [Errors vs Exceptions](https://link.springer.com/book/10.1007/978-3-031-50681-9)
Errors and exceptions are handled differently in a program. When an error occurs, the program terminates, and the error message is
displayed. In contrast, when an exception occurs, the program may continue running, and the code may handle the exception. Error handling usually requires the programmer to check for errors manually by inspecting error codes or return values. Exception handling, on the other hand, is done using try-catch blocks, which allow the program to handle exceptions in a centralized and structured manner

Errors can have severe consequences for the program and its users. Errors can result in data loss, system downtime, or other negative outcomes. In contrast, exceptions can be handled without significantly affecting the program’s overall operation or stability. By handling exceptions gracefully, programs can provide a better user experience and minimize the impact of errors on the system.


Advantages of error codes/return values:
Performance: Exception handling can introduce a performance overhead, especially if exceptions are thrown frequently. Error codes, being a more direct mechanism, generally have a predictable and often lower overhead.

Explicit control flow: With error codes, the control flow is explicit. A function returns an error, and the caller explicitly checks this return value.


Disadvantages of error codes/return values:
Verbose: You have to check after every call if an error occurs. This can lead to a
lot of repetitive code, making the source code longer and harder to read.


Mistakes: It’s easy to forget to check an error code. If this happens, the program
will continue running as if nothing went wrong, possibly leading to more issues
down the line.


In our system, we have already established a way to create and return error codes.


If we can recover and continue or choose an alternative path, it's an exception. If we must abort or inform the user, it's an error and needs a code.

## Principles:
### [Throw early, catch late](https://dev.to/davo_man/handling-exceptions-in-java-the-throw-early-catch-late-principle-12p0)
Throw exceptions as soon as they occur and catch them at an appropriate level in the code.
Why do this?

Throwing early benefits:
1. Improved code readability: Throwing exceptions closer to their occurrence helps in understanding the flow of the code and makes it easier to identify potential error scenarios.
2. Precise error reporting: Throwing exceptions early allows for more accurate error reporting, as the exception is thrown at the point where the problem is detected.
3. Separation of concerns: Throwing exceptions early ensures that the code responsible for handling exceptional cases is separate from the normal code flow. This promotes better organization and maintainability of the codebase.

Catching Exceptions Late:
Catching exceptions at a higher level in the call stack, closer to the point where the exception can be appropriately handled. Consider the following reasons for catching exceptions late:
1. Centralized exception handling: Catching exceptions at a higher level allows for centralized exception handling, which can be particularly useful in scenarios where multiple methods or components may encounter similar exceptions.
2. Enhanced error recovery: Catching exceptions at an appropriate level provides an opportunity to recover from the exceptional situation or take corrective actions, such as providing alternative processing paths or presenting user-friendly error messages.
3. Avoiding code duplication: By catching exceptions at a higher level, we can avoid duplicating exception handling logic across multiple methods or components, resulting in more concise and maintainable code.

Also: https://softwareengineering.stackexchange.com/questions/231057/exceptions-why-throw-early-why-catch-late

In summary:
1. Identify the specific exceptions that can occur in a method and throw them immediately when the exceptional condition arises.

2. Choose an appropriate level in the call stack to catch the exceptions based on the context and the ability to handle or recover from them effectively.

3. Avoid catching exceptions at a level that cannot handle or recover from them properly. Instead, let the exception propagate to a higher level where it can be appropriately dealt with.

4. Provide meaningful error messages and additional contextual information when throwing exceptions to aid in troubleshooting and debugging.


### [5 Best Practices for Python Exception Handling](https://medium.com/@saadjamilakhtar/5-best-practices-for-python-exception-handling-5e54b876a20)

1. Use Specific Exceptions: Instead of relying on a generic catch-all statement, it’s essential to catch specific exception types.
2. Implement Error Logging (use LOGGER.error)
3. Define Custom Exception Classes
4. Handle Exceptions Gracefully (set task state to error, inform user,)
5. Use Finally for Cleanup Tasks

### Do not return None ([Effective Python Item 20](https://effectivepython.com/))
Prefer raising an exception instead of returning None.
Interpreting None as false (`if not result` when result is None) can lead to errors. Also, missing a None check can lead to errors. It is best to raise an exception.

* Functions that return None to indicate special meaning are error
prone because None and other values (e.g., zero, the empty string)
all evaluate to False in conditional expressions.

* Raise exceptions to indicate special situations instead of returning
None. Expect the calling code to handle exceptions properly when
they’re documented.

```python
def careful_divide(a: float, b: float) -> float:
"""Divides a by b

Raises:
    ValueError: When the inputs cannot be divided
"""
try:
    return a / b
except ZeroDivisionError as e:
    raise ValueError("Invalid inputs")
```

### [Additional considerations](https://ioflood.com/blog/python-exception/)
* Always catch exceptions that you know how to handle and let the rest propagate up.
* Use the most specific exceptions possible in your except clauses.
* Avoid catching the base Exception class, as this can mask real issues in your code.

## Plan of actions

### Custom exceptions
Create custom exceptions file in `compute.common.exception` folder (there is only error this far)

### Unhandled errors
1. Scan for unhandled errors
2. Surround throwing code with try-catch-finally (if needed) blocks
3. Catch only specific exception
4. Handle the exception locally (set error state, return status, etc) if you have enough context and capabilities to do so. Otherwise propagate it up so that the caller handles it.


### None returns
Our database code is full of `return None` statements
1. Search for `return None`
2. Replace with throw `DatabaseElementNotFoundError` (custom exception)
3. Handle with try-except-finally(if needed) where the database wrapper method is called. All database methods that retrieve data should have this.

### Add logs to exceptions
Ensure all exceptions have a log.
1. Be specific: Clearly describe the error condition and its consequences.
2. Be actionable: Provide guidance on how to resolve the issue or recover from the
error, if possible.
3. Be concise: Keep the message short and focused, avoiding unnecessary jargon
and verbosity.
4. Be user-friendly: Use clear, natural language that is appropriate for the target
audience, whether it’s developers, end users, or both.
5. Be consistent: Follow a consistent style and format for error messages throughout
the application, making them easier to recognize and understand.