### Azure
* [Describe the core architectural components of Azure](https://learn.microsoft.com/en-us/training/modules/describe-core-architectural-components-of-azure/)
* [Describe Azure storage services](https://learn.microsoft.com/en-us/training/modules/describe-azure-storage-services/)
* [https://learn.microsoft.com/en-us/training/modules/aks-app-scale-keda/](https://learn.microsoft.com/en-us/training/modules/aks-app-scale-keda/)

### Kubernetes
* [Introduction to Kubernetes](https://learn.microsoft.com/en-us/training/modules/intro-to-kubernetes/)

### AKS
* [Introduction to Azure Kubernetes Service](https://learn.microsoft.com/en-us/training/modules/intro-to-azure-kubernetes-service/)
* [Configure Azure Kubernetes Service](https://learn.microsoft.com/en-us/training/modules/configure-azure-kubernetes-service/)
* [Azure Kubernetes Service cluster using Container insights in Azure Monitor](https://learn.microsoft.com/en-us/training/modules/aks-monitor/)
